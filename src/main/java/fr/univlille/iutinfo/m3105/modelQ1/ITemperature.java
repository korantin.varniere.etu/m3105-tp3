package fr.univlille.iutinfo.m3105.modelQ1;

import fr.univlille.iutinfo.m3105.modelQ2.Echelle;

public interface ITemperature {
	public double getTemperature();
	public void setTemperature(double val);

	public void incrementTemperature();
	public void decrementTemperature();
	
	public Echelle getEchelle();

}
