package fr.univlille.iutinfo.m3105.modelQ1;

import fr.univlille.iutinfo.m3105.modelQ2.Echelle;
import fr.univlille.iutinfo.m3105.utils.Subject;

public class Thermogeekostat extends Subject implements ITemperature {
	public static final double DEFAULT_VALUE =  18.0;
	
	protected double wishedTemp;
	
	public Thermogeekostat() {
		wishedTemp = DEFAULT_VALUE;
	}

	@Override
	public double getTemperature() {
		return wishedTemp;
	}

	@Override
	public void setTemperature(double val) {
		wishedTemp	 = val;
		notifyObservers(wishedTemp);
	}

	@Override
	public void incrementTemperature() {
		wishedTemp++;
		notifyObservers(wishedTemp);
	}

	@Override
	public void decrementTemperature() {
		wishedTemp--;
		notifyObservers(wishedTemp);
	}

	@Override
	public Echelle getEchelle() {
		return null;
	}

}
