package fr.univlille.iutinfo.m3105.viewQ1;

import fr.univlille.iutinfo.m3105.modelQ1.Thermogeekostat;
import fr.univlille.iutinfo.m3105.utils.Observer;
import fr.univlille.iutinfo.m3105.utils.Subject;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

public abstract class AbstractVue extends Stage implements Observer, ITemperatureView {

	protected Thermogeekostat modele;

	public AbstractVue(Thermogeekostat temp) {
		this.modele = temp;
		this.modele.attach(this);
		
		Scene sc = new Scene(createSceneContent());
		this.setScene(sc);
		this.setTitle("Thermogeekostat");
		
		update(null, null);  // initialize display to current modele value
		
		this.show();
	}

	protected abstract Region createSceneContent();

	@Override
	public void update(Subject subj) {
		update(subj, null);		
	}

	@Override
	public void update(Subject subj, Object data) {
		setDisplayedValue(modele.getTemperature());
	}

	protected Button makeButton(String txt, EventHandler<ActionEvent> hdlr) {
		Button b;
		b = new Button(txt);
		b.setMinWidth(Region.USE_PREF_SIZE);
		b.setPrefWidth(40.0);
		b.setOnAction( hdlr );
		return b;
	}

	@Override
	public abstract double getDisplayedValue();

	@Override
	public abstract void setDisplayedValue(double val);

	@Override
	public void incrementAction() {
		modele.incrementTemperature();
	}

	@Override
	public void decrementAction() {
		modele.decrementTemperature();
	}

}