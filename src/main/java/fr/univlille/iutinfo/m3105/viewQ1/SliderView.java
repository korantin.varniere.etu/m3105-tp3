package fr.univlille.iutinfo.m3105.viewQ1;

import fr.univlille.iutinfo.m3105.modelQ1.Thermogeekostat;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Slider;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class SliderView extends AbstractVue {
	protected Slider saisie;

	public SliderView(Thermogeekostat temp) {
		super(temp);
	}

	@Override
	protected Region createSceneContent() {
		VBox vb = new VBox();
		vb.setAlignment(Pos.CENTER);
		vb.setPadding(new Insets(5, 10, 5, 10));
		vb.setSpacing(10);

		saisie = createSlider();
		vb.getChildren().addAll(
				makeButton("+", (e) -> incrementAction()),
				saisie,
				makeButton("-", (e) -> decrementAction()) );
		
		return vb;
	}

	private Slider createSlider() {
		Slider slid;
		int min = -10;
		int max = 50;

		slid = new Slider(min,max,0);
		slid.setOrientation(Orientation.VERTICAL);
		slid.setMajorTickUnit( 10 );
		slid.setShowTickMarks(true);
		slid.setShowTickLabels(true);
        
		slid.valueProperty().addListener( (e,o,n) -> modele.setTemperature((double)n) );

		return slid;
	}

	@Override
	public double getDisplayedValue() {
		return saisie.getValue();
	}

	@Override
	public void setDisplayedValue(double val) {
		saisie.setValue( val );
	}

}
