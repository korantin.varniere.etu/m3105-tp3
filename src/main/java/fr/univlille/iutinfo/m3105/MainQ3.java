package fr.univlille.iutinfo.m3105;

import fr.univlille.iutinfo.m3105.modelQ2.Echelle;
import fr.univlille.iutinfo.m3105.modelQ2.Temperature;
import fr.univlille.iutinfo.m3105.modelTP3.Timer;
import fr.univlille.iutinfo.m3105.viewQ3.SliderView;
import fr.univlille.iutinfo.m3105.viewQ3.TextView;
import fr.univlille.iutinfo.m3105.viewTP3.ChooseView;
import javafx.application.Application;
import javafx.stage.Stage;

public class MainQ3 extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Temperature tempC = new Temperature(Echelle.CELSIUS);
		tempC.setTemperature(18.0);
		Temperature tempF = new Temperature(Echelle.FAHRENHEIT);
		tempF.biconnectTo(tempC);
		Temperature tempN = new Temperature(Echelle.NEWTON);
		tempN.biconnectTo(tempF);
		Temperature tempK = new Temperature(Echelle.KELVIN);
		tempK.biconnectTo(tempN);
		Temperature tempR = new Temperature(Echelle.RANKINE);
		tempR.biconnectTo(tempK);
		
		
		Timer timer = new Timer(tempC);
		tempC.attach(timer);
		
		timer.start();
		
		new ChooseView(tempC, tempF, tempN, tempK, tempR);
	}

}
