package fr.univlille.iutinfo.m3105.viewTP3;

import fr.univlille.iutinfo.m3105.modelQ2.Temperature;
import fr.univlille.iutinfo.m3105.viewQ3.SliderView;
import fr.univlille.iutinfo.m3105.viewQ3.TextView;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ChooseView extends Stage {

	public ChooseView(Temperature tempC, Temperature tempF, Temperature tempN, Temperature tempK, Temperature tempR) {
		VBox root = new VBox();
		
		HBox celsius = new HBox();
		
		Label cL = new Label("Celsius: ");
		Button cT = new Button("TextView");
		cT.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				new TextView(tempC);
			}
			
		});
		Button cS = new Button("SliderView");
		cS.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				new SliderView(tempC);
			}
			
		});
		
		celsius.getChildren().addAll(cL, cT, cS);
		
		HBox fahrenheit = new HBox();
		
		Label fL = new Label("Fahrenheit: ");
		Button fT = new Button("TextView");
		fT.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				new TextView(tempF);
			}
			
		});
		Button fS = new Button("SliderView");
		fS.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				new SliderView(tempF);
			}
			
		});
		
		fahrenheit.getChildren().addAll(fL, fT, fS);
		
		HBox kelvin = new HBox();
		
		Label kL = new Label("Kelvin: ");
		Button kT = new Button("TextView");
		kT.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				new TextView(tempK);
			}
			
		});
		Button kS = new Button("SliderView");
		kS.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				new SliderView(tempK);
			}
			
		});
		
		kelvin.getChildren().addAll(kL, kT, kS);
		
		HBox newton = new HBox();
		
		Label nL = new Label("Newton: ");
		Button nT = new Button("TextView");
		nT.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				new TextView(tempN);
			}
			
		});
		Button nS = new Button("SliderView");
		nS.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				new SliderView(tempN);
			}
			
		});
		
		newton.getChildren().addAll(nL, nT, nS);
		
		HBox rankine = new HBox();
		
		Label rL = new Label("Rankine: ");
		Button rT = new Button("TextView");
		rT.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				new TextView(tempR);
			}
			
		});
		Button rS = new Button("SliderView");
		rS.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				new SliderView(tempR);
			}
			
		});
		
		rankine.getChildren().addAll(rL, rT, rS);
		
		root.getChildren().addAll(celsius, fahrenheit, kelvin, newton, rankine);
		
		Scene scene = new Scene(root);
		
		this.setScene(scene);
		this.setTitle("Choose Temp");
		this.show();
	}
	
}
