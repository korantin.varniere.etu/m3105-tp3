package fr.univlille.iutinfo.m3105.modelQ2;

import fr.univlille.iutinfo.m3105.modelQ1.ITemperature;
import fr.univlille.iutinfo.m3105.utils.ConnectableProperty;
import fr.univlille.iutinfo.m3105.utils.Subject;

/** Stores a temperature (value) + knows in what temperature scale it is expressed
 */
public class Temperature extends ConnectableProperty implements ITemperature {

	/** the scale in which the temperature is stored
	 */
	protected Echelle echelle;

	public Temperature(Echelle e) {
		super();
		echelle = e;
	}

	@Override
	public void incrementTemperature() {
		setValue( (double)value + 1);
	}

	@Override
	public void decrementTemperature() {
		setValue( (double)value - 1);
	}

	public Echelle getEchelle() {
		return echelle;
	}

	@Override
	public void update(Subject other, Object value) {
		Echelle otherEchelle = ((Temperature) other).getEchelle(); 
		setValue( echelle.fromKelvin( otherEchelle.toKelvin((double) value)) );
	}

	@Override
	public double getTemperature() {
		return (double)this.getValue();
	}

	@Override
	public void setTemperature(double val) {
		this.setValue(val);
	}

}
