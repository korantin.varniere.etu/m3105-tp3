package fr.univlille.iutinfo.m3105.viewQ3;

import fr.univlille.iutinfo.m3105.modelQ2.Echelle;
import fr.univlille.iutinfo.m3105.modelQ2.Temperature;
import fr.univlille.iutinfo.m3105.utils.Observer;
import fr.univlille.iutinfo.m3105.utils.Subject;
import fr.univlille.iutinfo.m3105.viewQ1.ITemperatureView;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public abstract class AbstractVue extends Stage implements Observer, ITemperatureView {

	protected Temperature modele;
	protected String title;

	public AbstractVue(Temperature temp) {
		this.modele = temp;
		this.modele.attach(this);
		
		Scene sc = new Scene(createSceneContent());
		this.setScene(sc);
		this.setTitle(getEchelle().getName());
		
		update(temp);  // initialize display to current model value
		
		this.show();
	}

	protected abstract Region createSceneContent();

	@Override
	public void update(Subject subj) {
		update(subj, null);		
	}

	@Override
	public void update(Subject subj, Object data) {
		setDisplayedValue(modele.getTemperature());
	}

	protected Button makeButton(String txt, EventHandler<ActionEvent> hdlr) {
		Button b;
		b = new Button(txt);
		b.setMinWidth(Region.USE_PREF_SIZE);
		b.setPrefWidth(40.0);
		b.setOnAction( hdlr );
		return b;
	}

	@Override
	public abstract double getDisplayedValue();

	@Override
	public abstract void setDisplayedValue(double val);

	@Override
	public void incrementAction() {
		modele.incrementTemperature();
	}

	@Override
	public void decrementAction() {
		modele.decrementTemperature();
	}

	public Echelle getEchelle() {
		return modele.getEchelle();
	}

	protected VBox createVBox() {
		VBox vb = new VBox();
		vb.setAlignment(Pos.CENTER);
		vb.setPadding(new Insets(5, 10, 5, 10));
		vb.setSpacing(10);
		return vb;
	}

}