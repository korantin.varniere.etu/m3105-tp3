package fr.univlille.iutinfo.m3105.viewQ3;

import fr.univlille.iutinfo.m3105.modelQ2.Echelle;
import fr.univlille.iutinfo.m3105.modelQ2.Temperature;
import javafx.geometry.Orientation;
import javafx.scene.control.Slider;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class SliderView extends AbstractVue {
	protected Slider saisie;

	public SliderView(Temperature temp) {
		super(temp);
	}

	@Override
	protected Region createSceneContent() {
		VBox vb = createVBox();

		saisie = createSlider();
		vb.getChildren().addAll(
				makeButton("+", (e) -> incrementAction()),
				saisie,
				makeButton("-", (e) -> decrementAction()) );
		
		return vb;
	}

	private Slider createSlider() {
		Slider slid;
		int min = (int) getEchelle().fromKelvin( Echelle.CELSIUS.toKelvin(-10.0) );
		int max = (int) getEchelle().fromKelvin( Echelle.CELSIUS.toKelvin(50.0) );

		slid = configureSlider(min, max);
		slid.valueProperty().addListener( (e,o,n) -> modele.setTemperature((double)n) );

		return slid;
	}

	private Slider configureSlider(int min, int max) {
		Slider slid;
		slid = new Slider(min,max,0);
		slid.setOrientation(Orientation.VERTICAL);
		slid.setMajorTickUnit( (max - min) / 10 );
		slid.setShowTickMarks(true);
		slid.setShowTickLabels(true);
		return slid;
	}

	@Override
	public double getDisplayedValue() {
		return saisie.getValue();
	}

	@Override
	public void setDisplayedValue(double val) {
		saisie.setValue( val );
	}

}
